package acceptanceLoyaltySuite;

import objects.Template;
import org.testng.annotations.DataProvider;

/**
 * Created by user
 */
public class DataProviders {
	@DataProvider(name = "templates")

	public static Object[][] templates() {
		return new Object[][] {
				{ new Template("properties/templates/loyaltyTemplate.properties")},
				{ new Template("properties/templates/loyaltyTemplateVendor.properties")}
		};
	}
}
