package acceptanceLoyaltySuite;

import base.BaseTestCase;
import lombok.extern.log4j.Log4j;
import objects.Template;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import pageobjects.CartPage;
import pageobjects.CheckoutPage;
import pageobjects.HeaderPage;
import pageobjects.LoyaltyCabinetPage;
import services.Payment;

import static acceptanceLoyaltySuite.Help.user;
import static microservices.LoyaltyService.getBalanceByUser;
import static org.openqa.selenium.support.PageFactory.initElements;
import static services.Constants.ACCOUNT_LOYALTY_PHP;
import static services.CookieService.clearCookies;
import static services.ObjectService.getPriceDouble;
import static services.ReportService.assertEquals;
import static services.UrlService.getDirectlyURL;
import static services.UrlService.getURL;
import static services.WaiterService.waitForElementVisible;

/**
 * Created by user
 */
@Log4j
public class Loyalty_003_PayByBalanceTestCase extends BaseTestCase {

	@Test(dataProviderClass = DataProviders.class, dataProvider = "templates")
	public void test_003(Template template){

		double balanceBefore = getBalanceByUser(user);

		getURL("", driver);

		HeaderPage headerPage= initElements(driver, HeaderPage.class);
		headerPage.login(Help.user.getUserName(), Help.user.getPassword());

		CartPage cartPage = initElements(driver, CartPage.class);
		cartPage.clearCart(driver);

		cartPage.addToCartById(template.getTemplateID());
		waitForElementVisible(cartPage.cashBackPriceElement, driver);

		int priceTemplate = cartPage.getTotalCartPrice();

		cartPage.clickCheckoutNowButton();

		CheckoutPage checkoutPage = initElements(driver, CheckoutPage.class);

		checkoutPage.activateBonus();
		checkoutPage.payBy(Payment.bonus);

		//Go to loyalty page.
		LoyaltyCabinetPage loyaltyCabinetPage = initElements(driver, LoyaltyCabinetPage.class);
		getDirectlyURL(ACCOUNT_LOYALTY_PHP, driver);
		waitForElementVisible(loyaltyCabinetPage.balanceValue, driver);

		/**
		 * Checking
		 */
		//From api.
		double balanceFromCabinet = getPriceDouble(loyaltyCabinetPage.balanceValue);
		assertEquals(balanceFromCabinet, getBalanceByUser(user),
				"Balance does not match with expected.");

		//From frontend.
		assertEquals(balanceFromCabinet, balanceBefore-priceTemplate, "Balance does not match with expected.");
	}

	@AfterMethod
	public void tearDown() {
		CartPage cartPage = initElements(driver, CartPage.class);
		cartPage.clearCart(driver);
		clearCookies(driver);
	}
}
