package acceptanceLoyaltySuite;

import objects.Offer;
import objects.Order;
import objects.Template;
import objects.User;

/**
 * Created by user
 */
public interface Help {
	Template templateLoyalty = new Template("properties/templates/loyaltyTemplate.properties");
	Order approveOrder = new Order("properties/orders/approvedOrder.properties");
	Order canceledOrder = new Order("properties/orders/canceledOrder.properties");
	User user = new User("properties/users/ukrGuest.properties");
	User userWithoutBonuses = new User("properties/users/userWithoutBonuses.properties");
	User userDevoffice = new User("properties/users/devGuest.properties");
	User salesAdmin = new User("properties/users/salesBillingAdmin.properties");
	String subjectLetterPart = "onuses were";
	Offer onCartOffer = new Offer("properties/offer/onCart.properties");
	int partBonus = 10;
}
