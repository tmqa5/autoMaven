package acceptanceLoyaltySuite;

import base.BaseTestCase;
import lombok.extern.log4j.Log4j;
import org.testng.annotations.Test;
import pageobjects.*;
import services.Payment;
import services.UrlService;

import static acceptanceLoyaltySuite.Help.approveOrder;
import static acceptanceLoyaltySuite.Help.user;
import static microservices.LoyaltyService.getBalanceByUser;
import static org.openqa.selenium.support.PageFactory.initElements;
import static services.Constants.*;
import static services.ObjectService.getPriceDouble;
import static services.OrderService.getLastOrderIdUserName;
import static services.ReportService.assertEquals;
import static services.UrlService.getDirectlyURL;
import static services.UrlService.getURL;
import static services.WaiterService.waitForElementVisible;
import static services.WaiterService.waitPageLoader;

/**
 * Created by user
 */
@Log4j
public class Loyalty_007_BuyOnCartOfferTestCase extends BaseTestCase {

	@Test
	public void test_007(){

		double balanceBefore = getBalanceByUser(user);

		getURL("", driver);

		HeaderPage headerPage= initElements(driver, HeaderPage.class);
		headerPage.login(Help.user.getUserName(), Help.user.getPassword());

		CartPage cartPage = initElements(driver, CartPage.class);
		cartPage.clearCart(driver);

		cartPage.addOfferById(Help.onCartOffer.getId());
		waitForElementVisible(cartPage.cashBackPriceElement, driver);

		double cacheBack = getPriceDouble(cartPage.cashBackPriceElement);

		cartPage.clickCheckoutNowButton();

		CheckoutPage checkoutPage = initElements(driver, CheckoutPage.class);
		checkoutPage.payBy(Payment.PayPal);
		waitPageLoader("paypal", PAGE_TIMEOUT, driver);

		approveOrder.setId(getLastOrderIdUserName(Help.user));

		//Navigate to sales-billing page.
		UrlService.getURL(SALES_BILLING_URL,driver);
		//Search order.
		SalesBillingPage salesBillingPage = initElements(driver, SalesBillingPage.class);
		salesBillingPage.login(Help.salesAdmin.getUserName(), Help.salesAdmin.getPassword());
		salesBillingPage.submitOrderId(approveOrder.getId());
		salesBillingPage.clickOnSearchButton();
		//Approved order.
		salesBillingPage.changeOrderStatus(approveOrder.getId(), approveOrder.getAction(),
				approveOrder.getReason(), approveOrder.getReasonText());

		//Go to loyalty page.
		LoyaltyCabinetPage loyaltyCabinetPage = initElements(driver, LoyaltyCabinetPage.class);
		getDirectlyURL(ACCOUNT_LOYALTY_PHP, driver);
		waitForElementVisible(loyaltyCabinetPage.balanceValue, driver);

		/**
		 * Checking
		 */
		//From api.
		double balanceFromCabinet = getPriceDouble(loyaltyCabinetPage.balanceValue);
		assertEquals(balanceFromCabinet, getBalanceByUser(user), "Balance does not match with expected.");

		//From frontend.
		double sum = balanceBefore+cacheBack;
		assertEquals(balanceFromCabinet, sum, "Balance does not match with expected.");

	}
}
