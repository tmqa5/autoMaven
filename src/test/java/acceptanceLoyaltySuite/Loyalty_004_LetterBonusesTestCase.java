package acceptanceLoyaltySuite;

import base.BaseTestCase;
import lombok.extern.log4j.Log4j;
import org.testng.annotations.Test;
import pageobjects.CartPage;
import pageobjects.CheckoutPage;
import pageobjects.HeaderPage;
import pageobjects.SalesBillingPage;
import services.Payment;
import services.UrlService;
import services.WaiterService;

import static acceptanceLoyaltySuite.Help.*;
import static org.openqa.selenium.support.PageFactory.initElements;
import static services.Constants.PAGE_TIMEOUT;
import static services.Constants.SALES_BILLING_URL;
import static services.MailService.verifyLetterContent;
import static services.ObjectService.getPriceDouble;
import static services.OrderService.getLastOrderIdUserName;
import static services.ReportService.assertTrue;
import static services.UrlService.getURL;
import static services.WaiterService.waitForElementVisible;
import static services.WaiterService.waitPageLoader;

/**
 * Created by user
 */
@Log4j
public class Loyalty_004_LetterBonusesTestCase extends BaseTestCase {

	@Test
	public void test_004(){

		getURL("", driver);

		HeaderPage headerPage= initElements(driver, HeaderPage.class);
		headerPage.login(userDevoffice.getUserName(), userDevoffice.getPassword());

		CartPage cartPage = initElements(driver, CartPage.class);
		cartPage.clearCart(driver);

		cartPage.addToCartById(Help.templateLoyalty.getTemplateID());
		waitForElementVisible(cartPage.cashBackPriceElement, driver);

		double cacheBack = getPriceDouble(cartPage.cashBackPriceElement);

		cartPage.clickCheckoutNowButton();

		CheckoutPage checkoutPage = initElements(driver, CheckoutPage.class);
		checkoutPage.payBy(Payment.PayPal);
		waitPageLoader("paypal", PAGE_TIMEOUT, driver);

		approveOrder.setId(getLastOrderIdUserName(userDevoffice));

		//Navigate to sales-billing page.
		UrlService.getURL(SALES_BILLING_URL,driver);
		//Search order.
		SalesBillingPage salesBillingPage = initElements(driver, SalesBillingPage.class);
		salesBillingPage.login(Help.salesAdmin.getUserName(), Help.salesAdmin.getPassword());
		salesBillingPage.submitOrderId(approveOrder.getId());
		salesBillingPage.clickOnSearchButton();
		//Approved order.
		salesBillingPage.changeOrderStatus(approveOrder.getId(), approveOrder.getAction(),
				approveOrder.getReason(), approveOrder.getReasonText());

		/**
		 * Checking
		 */
		//Verify Letters.
		WaiterService.waitForLetterContent(subjectLetterPart, approveOrder.getId(), driver);
		assertTrue(verifyLetterContent(subjectLetterPart, String.valueOf(cacheBack)),
				"User didn't receive letter.");
	}
}
