package acceptanceLoyaltySuite;

import lombok.extern.log4j.Log4j;
import org.testng.annotations.Test;
import pageobjects.LoyaltyCabinetPage;
import pageobjects.SalesBillingPage;
import services.UrlService;

import static acceptanceLoyaltySuite.Help.approveOrder;
import static acceptanceLoyaltySuite.Help.canceledOrder;
import static org.openqa.selenium.support.PageFactory.initElements;
import static services.Constants.ACCOUNT_LOYALTY_PHP;
import static services.Constants.SALES_BILLING_URL;
import static services.ObjectService.getPriceDouble;
import static services.ReportService.assertEquals;
import static services.UrlService.getDirectlyURL;
import static services.UrlService.switchToFrame;
import static services.WaiterService.waitForElementVisible;

/**
 * Created by user
 */
@Log4j
public class Loyalty_002_CancelBonusesTestCase extends Loyalty_001_ReturnUserBonusesTestCase {

	@Test
	public void test_002(){
		//Navigate to sales-billing page.
		UrlService.getURL(SALES_BILLING_URL,driver);
		//Search order.
		SalesBillingPage salesBillingPage = initElements(driver, SalesBillingPage.class);
		salesBillingPage.submitOrderId(approveOrder.getId());
		salesBillingPage.clickOnSearchButton();
		//Cancel order.
		canceledOrder.setId(approveOrder.getId());
		salesBillingPage.changeOrderStatus(canceledOrder.getId(), canceledOrder.getAction(),
				canceledOrder.getReason(), canceledOrder.getReasonText());
		//Verify status.
		switchToFrame("main", driver);
		switchToFrame("mainContent", driver);

		//Go to loyalty page.
		LoyaltyCabinetPage loyaltyCabinetPage = initElements(driver, LoyaltyCabinetPage.class);
		getDirectlyURL(ACCOUNT_LOYALTY_PHP, driver);
		waitForElementVisible(loyaltyCabinetPage.balanceValue, driver);

		/**
		 * Checking (created bug - https://gitlab.com/tmqa5/autoMaven/issues/1)
		 */
		//From frontend.
		assertEquals(getPriceDouble(loyaltyCabinetPage.balanceValue), sum-cacheBack,
				"Balance does not match with expected.");

	}
}
