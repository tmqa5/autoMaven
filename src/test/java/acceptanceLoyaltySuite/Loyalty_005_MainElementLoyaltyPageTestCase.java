package acceptanceLoyaltySuite;

import base.BaseTestCase;
import lombok.extern.log4j.Log4j;
import org.testng.annotations.Test;
import pageobjects.HeaderPage;
import pageobjects.LoyaltyCabinetPage;

import static acceptanceLoyaltySuite.Help.user;
import static org.openqa.selenium.support.PageFactory.initElements;
import static services.Constants.ACCOUNT_LOYALTY_PHP;
import static services.ReportService.assertTrue;
import static services.UrlService.getDirectlyURL;
import static services.UrlService.getURL;
import static services.WebElementService.elementIsDisplayed;

/**
 * Created by user
 */
@Log4j
public class Loyalty_005_MainElementLoyaltyPageTestCase extends BaseTestCase {

	@Test
	public void test_005(){
		getURL("", driver);
		HeaderPage headerPage= initElements(driver, HeaderPage.class);
		headerPage.login(user.getUserName(), user.getPassword());

		//Go to loyalty page.
		getDirectlyURL(ACCOUNT_LOYALTY_PHP, driver);

		/**
		 * Checking
		 */
		LoyaltyCabinetPage loyaltyCabinetPage= initElements(driver, LoyaltyCabinetPage.class);
		assertTrue(elementIsDisplayed(loyaltyCabinetPage.balanceValue, "balanceValue"),
				"balanceValue is not displayed.");

		//Sometimes will be problem with infoAboutBonuses (micro service order is wrong)
		assertTrue(elementIsDisplayed(loyaltyCabinetPage.infoAboutBonuses, "infoAboutBonuses"),
				"infoAboutBonuses is not displayed.");

		assertTrue(elementIsDisplayed(loyaltyCabinetPage.levelLink, "levelLink"),
				"levelLink is not displayed.");
		assertTrue(elementIsDisplayed(loyaltyCabinetPage.loyaltyStatistic, "loyaltyStatistic"),
				"loyaltyStatistic is not displayed.");
		assertTrue(elementIsDisplayed(loyaltyCabinetPage.progreessToolbar, "progreessToolbar"),
				"progreessToolbar is not displayed.");
		assertTrue(elementIsDisplayed(loyaltyCabinetPage.termsTable, "termsTable"),
				"termsTable is not displayed.");
		assertTrue(elementIsDisplayed(loyaltyCabinetPage.questButton, "questButton"),
				"questButton is not displayed.");

	}
}
