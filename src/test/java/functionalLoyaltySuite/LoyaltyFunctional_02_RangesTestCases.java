package functionalLoyaltySuite;

import base.BaseFunctionalTestCase;
import lombok.extern.log4j.Log4j;
import objects.Range;
import org.testng.annotations.Test;

import java.util.List;

import static microservices.LoyaltyService.getLevels;
import static microservices.LoyaltyService.verifyInterval;
import static services.ObjectService.random;
import static services.ReportService.assertTrue;

/**
 * Created by user
 */
@Log4j
public class LoyaltyFunctional_02_RangesTestCases  extends BaseFunctionalTestCase {

	List<Range> ranges = getLevels().get(0).getRanges();

	@Test
	public void valueRange_001() {
		for (Range range:ranges){
			assertTrue(range.getValue()>0, "Range value <= 0.");
			assertTrue(range.getValue()<100, "Range value >= 100.");
		}
	}

	@Test
	public void fromRange_002() {
		for (Range range:ranges){
			assertTrue(range.getFrom()>=0, "Range from < 0.");
		}
	}

	@Test
	public void toRange_003() {
		for (Range range:ranges){
			assertTrue(range.getTo()>0, "Range from <= 0.");
		}
	}

	@Test
	public void intervalRange_004() {
		for (Range range:ranges){
			int rnd = random(range.getFrom(), range.getTo());
			assertTrue(verifyInterval(rnd, range.getFrom(), range.getTo()),
					rnd + " not included in the interval - " + range);
		}
	}

	@Test
	public void fromToRange_005() {
		for (Range range:ranges){
			assertTrue(range.getFrom()<=range.getTo(), "Range can not be negative. " + range);
		}
	}

}
