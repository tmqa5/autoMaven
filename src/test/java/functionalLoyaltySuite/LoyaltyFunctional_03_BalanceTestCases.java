package functionalLoyaltySuite;

import acceptanceLoyaltySuite.Help;
import base.BaseFunctionalTestCase;
import lombok.extern.log4j.Log4j;
import org.testng.annotations.Test;

import static microservices.LoyaltyService.getBalanceByUser;
import static services.ReportService.assertEquals;
import static services.ReportService.assertTrue;

/**
 * Created by user
 */
@Log4j
public class LoyaltyFunctional_03_BalanceTestCases extends BaseFunctionalTestCase {

	double balanceOldUser = getBalanceByUser(Help.user);
	double balanceNewUser = getBalanceByUser(Help.userWithoutBonuses);

	@Test
	public void userWithBalance_001() {
		assertTrue(balanceOldUser>1, Help.user.getUserName() + " doesn't have bonuses.");
	}

	@Test
	public void newUserBalance_002() {
		assertEquals(balanceNewUser,0.0, "new user " + Help.userWithoutBonuses.getEmail() + " have bonuses.");
	}

}
