package functionalLoyaltySuite;

import base.BaseFunctionalTestCase;
import lombok.extern.log4j.Log4j;
import objects.Level;
import org.testng.annotations.Test;

import java.util.List;

import static microservices.LoyaltyService.getLevels;
import static services.ReportService.assertFalse;
import static services.ReportService.assertTrue;

/**
 * Created by user
 */
@Log4j
public class LoyaltyFunctional_01_LevelsTestCases extends BaseFunctionalTestCase {

	List<Level> levels = getLevels();

	@Test
	public void namesLevel_001() {
		for (Level level:levels){
			assertFalse(level.getName().isEmpty(), "Name of level is empty.");
		}
	}

	@Test
	public void sizeListLevel_002() {
		assertTrue(levels.size()>1, "Levels size does not match with expected.");
	}

	@Test
	public void ordersLevel_003() {
		for (Level level:levels){
			assertTrue(level.getOrders()>0, "Count of orders <= 0.");
		}
	}

	@Test
	public void idLevel_004() {
		for (Level level:levels){
			assertFalse(level.getId().isEmpty(), "Id of level is empty.");
		}
	}

	@Test
	public void rangesLevel_005() {
		for (Level level:levels){
			assertTrue(level.getRanges().size()>1, "Ranges of level <= 1.");
		}
	}
}
