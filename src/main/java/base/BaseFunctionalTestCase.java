package base;

import lombok.extern.log4j.Log4j;
import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

/**
 * Created by user
 */
@Log4j
public class BaseFunctionalTestCase {

	protected String testCaseName = this.getClass().getSimpleName();

	/**
	 * Steps before start suite
	 */
	@BeforeSuite(alwaysRun = true)
	public void startTestSuite() {
		System.setProperty("java.util.logging.config.file", "logging.properties");
		log.info("");
		log.info("=================================================================");
		log.info("Functional test suite started.");
		log.info("=================================================================");
		log.info("");
	}

	/**
	 * Steps before starting every test
	 */
	@BeforeTest
	public void startTest(){
		log.info("=================================================================");
		log.info("TestCase: \"" + testCaseName + "\" started.");
		log.info("=================================================================");
	}

	/**
	 * Steps after test
	 * @param context
	 */
	@AfterTest
	public void finishTest(ITestContext context) {
		log.info("=================================================================");
		log.info("TestCase: \"" + testCaseName + "\" finished.");
		log.info("=================================================================");
		log.info("");
	}

	/**
	 * Steps after whole suite
	 */
	@AfterSuite(alwaysRun = true)
	public void finishTestSuite() {
		log.info("=================================================================");
		log.info("Test suite finished.");
		log.info("=================================================================");
		log.info("");
	}
}
