package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by user
 */
public class LoyaltyCabinetPage extends AbstractPage {
	private WebDriver driver;
	public LoyaltyCabinetPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	@FindBy(xpath = "//*[contains(@class, 'UserBalance__value')]")
	public WebElement balanceValue;

	@FindBy(xpath = "//ul[contains(@class, 'ProgressBar__toolbar')]")
	public WebElement progreessToolbar;

	@FindBy(xpath = "//a[contains(@class, 'ProgressBar__descriptionLevel--userLevel')]")
	public WebElement levelLink;

	@FindBy(xpath = "//article[contains(@class, 'LoyaltyStatistic')]")
	public WebElement loyaltyStatistic;

	@FindBy(xpath = "//table[contains(@class, 'LoyaltyTableLevel')]")
	public WebElement termsTable;

	@FindBy(xpath = "//table[contains(@class, 'LoyaltyTableBonusesAdded')]")
	public WebElement infoAboutBonuses;

	@FindBy(xpath = "//a[contains(@class, 'LoyaltyFaq__lendingLink')]")
	public WebElement questButton;

}
