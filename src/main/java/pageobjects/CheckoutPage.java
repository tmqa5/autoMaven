package pageobjects;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import services.Payment;

import static services.Constants.*;
import static services.WaiterService.*;
import static services.WebElementService.*;

/**
 * Created by user
 */
@Log4j
public class CheckoutPage extends AbstractPage{
	private WebDriver driver;
	public CheckoutPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	@FindBy(xpath = "//button[contains(@class,'dbl-payment-popup')]")
	public WebElement doubleBilledPaymentPopupButton;

	@FindBy(xpath = "//article[@class='dbl-payment-popup']")
	public WebElement doubleBilledPaymentPopup;

	@FindBy(xpath = "//*[@for='bonus-check']")
	public WebElement bonusCheck;

	@FindBy(xpath = ".//*[contains(@id,'collapse-payment')]//a[contains(@id,'bonus')]")
	public WebElement payBonus;

	@FindBy(id = "bonuses3-form-bonus")
	public WebElement inputBonus;

	/**
	 * Pay by method
	 * @param paymentMethod PyaPal, Skrill, etc
	 */
	public void payBy(Payment paymentMethod) {
		if (paymentMethod.equals(Payment.bonus)){
			clickPayBonus();
			doubleBuyHack();
			waitPageLoader(CONTAINS_ORDERS_URL, driver);
		}
		else {
			pageLoaderWait(driver, PAGE_TIMEOUT);
			By element = By.xpath(".//*[contains(@id,'collapse-payment')]//a[contains(@id,'" + paymentMethod + "')]");
			waitForElementIsPresent(element, driver);
			WebElement payButtonElement = driver.findElement(element);
			moveToElement(payButtonElement, "payButtonElement", driver);
			waitForElementVisible(payButtonElement, driver);
			clickOnElement(payButtonElement, "By Now", driver);
			log.info("Pay with " + paymentMethod);
			doubleBuyHack();
		}
	}

	/**
	 * Huck for double billed order (if other order with template from present order is in buy)
	 */
	public void doubleBuyHack(){
		waitForElementVisible(doubleBilledPaymentPopup, ELEMENT_SMALL_TIMEOUT, driver);
		if(elementIsDisplayed(doubleBilledPaymentPopup, "Double billed popUp"))
			clickOnDoubleBilledPaymentPopupButton();
	}

	/**
	 * Hack for second billed
	 */
	public void clickOnDoubleBilledPaymentPopupButton() {
		clickOnElement(doubleBilledPaymentPopupButton, "continue button", driver);
	}

	public void activateBonus(){
		clickOnElement(bonusCheck, "bonusCheck", driver);
		waitForElementVisible(payBonus, driver);
	}

	public void clickPayBonus(){
		clickOnElement(payBonus, "payBonus", driver);
	}

	public void submitBonusValue(String value){
		clearFieldManual(inputBonus, driver, true);
		inputText(inputBonus, "inputBonus", value);
		clickOnElement(body, "body", driver);
	}

}
