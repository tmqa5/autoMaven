package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.PageFactory.initElements;
import static services.UrlService.switchToLastWindowClose;
import static services.WaiterService.waitForElementClickable;
import static services.WaiterService.waitForElementVisible;
import static services.WebElementService.clickOnElement;

/**
 * Created by user
 */
public class HeaderPage extends AbstractPage{
	private WebDriver driver;

	@FindBy(id = "menu-favorites")
	public WebElement collection;

	@FindBy(id = "header-signin-link")
	public WebElement signinLink;

	@FindBy(id = "user-avatar")
	public WebElement userAvatar;

	private AccountPage accountPage;

	public HeaderPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		accountPage = initElements(driver, AccountPage.class);
	}

	public void login(String email, String password) {
		clickSigninLink();
		waitForElementVisible(accountPage.email, driver);
		accountPage.login(email, password);
		waitForElementVisible(userAvatar, driver);
	}

	public void clickSigninLink() {
		waitForElementClickable(collection, driver);
		clickOnElement(signinLink, "Sign In Link", driver);
		switchToLastWindowClose(driver);
	}

}
