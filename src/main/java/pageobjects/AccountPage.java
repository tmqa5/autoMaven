package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static services.Constants.conf;
import static services.UrlService.getDirectlyURL;
import static services.WaiterService.waitForCookie;
import static services.WaiterService.waitForElementVisible;
import static services.WebElementService.clickOnElement;
import static services.WebElementService.inputText;

/**
 * Created by user
 */
public class AccountPage extends AbstractPage{
	private WebDriver driver;
	public AccountPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	@FindBy(id = "id-password-login-button")
	public WebElement signInButton;

	@FindBy(xpath = "//*[contains(@class,'general-email-field')]//input")
	public WebElement email;

	@FindBy(xpath = "//*[contains(@class, 'id-general-password-field')]/input")
	public WebElement password;

	@FindBy(id = "id-index-continue-button")
	public WebElement continueButton;

	/**
	 * Login user on account page
	 * @param emailText
	 * @param passwordText
	 */
	public void login(String emailText, String passwordText) {
		submitEmail(emailText);
		clickContinueButton();
		waitForElementVisible(password, driver);
		submitPassword(passwordText);
		clickSignInButton();

		waitForCookie("access_token", driver);
		getDirectlyURL(conf.url(), driver);
	}

	public void clickSignInButton(){
		clickOnElement(signInButton,"SignIn Button", driver);
	}

	public void submitEmail(String emailText){
		inputText(email, "Email", emailText);
	}

	public void submitPassword(String passwordText){
		inputText(password, "Password", passwordText);
	}

	public void clickContinueButton(){
		clickOnElement(continueButton, "continueButton", driver);
	}


}
