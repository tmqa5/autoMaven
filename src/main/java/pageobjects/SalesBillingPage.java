package pageobjects;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static services.Constants.ELEMENT_TIMEOUT;
import static services.Constants.PAGE_TIMEOUT;
import static services.UrlService.stopLoad;
import static services.UrlService.switchToFrame;
import static services.WaiterService.pageLoaderWait;
import static services.WaiterService.waitForElementVisible;
import static services.WebElementService.clickOnElement;
import static services.WebElementService.inputText;
import static services.WebElementService.selectDropBoxByText;

/**
 * Created by user
 */
@Log4j
public class SalesBillingPage extends AbstractPage{
	private WebDriver driver;
	public SalesBillingPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	@FindBy(id = "login")
	public WebElement userName;

	@FindBy (id = "password")
	public WebElement password;

	@FindBy (id = "submit")
	public WebElement submitButton;

	@FindBy (xpath = ".//form//input[@name='part_of_order_id']")
	public WebElement orderId;

	@FindBy (xpath = ".//*[@class='button_search']/button[@type='SUBMIT']")
	public  WebElement searchButton;

	@FindBy (xpath = ".//*[@id='click_me_on_load']/center")
	public WebElement controlBlock;

	@FindBy (xpath = ".//select[contains(@id,'orderForm_reason')]")
	public  WebElement orderReason;

	@FindBy (xpath = ".//input[@value='PROCEED']")
	public WebElement proceedButton;

	/**
	 * Login to /sales/admin
	 * @param login
	 * @param pass
	 */
	public void login(String login,String pass){
		try {
			inputText(userName,"UserName",login);
			inputText(password,"Password",pass);
			clickOnElement(submitButton, "Submit Button", driver);
			switchToFrame("main",driver);
			waitForElementVisible(searchButton,driver);
			switchToContent(driver);
		}
		catch (TimeoutException e){
			stopLoad(driver);
		}
	}

	public static void switchToContent(WebDriver driver){
		driver.switchTo().defaultContent();
		log.info("Switch to default content.");
	}

	public void submitOrderId(String id){
		switchToFrame("main",driver);
		inputText(orderId,"Order ID", id);
	}

	public void clickOnSearchButton(){
		clickOnElement(searchButton, "Search Button", driver);
		pageLoaderWait(driver, PAGE_TIMEOUT);
	}

	public void changeOrderStatus(String orderId, String status, String reason, String reasonText){
		clickOnControlBlock();
		selectStatus(orderId, status);
		submitReason(reason, reasonText);
		clickOnProceedButton();
	}

	public void clickOnControlBlock(){
		switchToFrame("mainContent",driver);
		clickOnElement(controlBlock, "Control Block", driver);

	}

	public void selectStatus(String orderId, String status){
		switchToFrame("actionFr_"+orderId,driver);
		WebElement statusButton = driver.findElement(By.className(status));
		clickOnElement(statusButton,status.toUpperCase()+" Button",driver);
		pageLoaderWait(driver, ELEMENT_TIMEOUT);

	}

	public void submitReason(String reason, String reasonText) {
		while (!orderReason.getAttribute("value").equals(reason)){
			selectDropBoxByText(orderReason,reasonText);
		}
	}

	public void clickOnProceedButton(){
		clickOnElement(proceedButton, "Proceed Button", driver);
		pageLoaderWait(driver, PAGE_TIMEOUT);
	}
}
