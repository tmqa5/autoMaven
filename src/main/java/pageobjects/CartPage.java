package pageobjects;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static services.Constants.*;
import static services.ObjectService.getPrice;
import static services.UrlService.getURL;
import static services.WaiterService.*;
import static services.WebElementService.clickOnElement;

/**
 * Created by user
 */
@Log4j
public class CartPage extends AbstractPage {
	private WebDriver driver;
	public CartPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	@FindBy(xpath = "//*[contains(@id, 'CartCashback__amountCashback--js')]")
	public WebElement cashBackPriceElement;

	@FindBy(xpath = "//*[contains(@id, 'CartCashback__userPercent--js')]")
	public WebElement cashBackPercentElement;

	@FindBy(xpath = ".//button[contains(@class,'checkout-button')]")
	public WebElement checkoutNowButton;

	@FindBy(xpath = "//div[contains(@class,'empty-cart')]")
	public WebElement clearCartIndex;

	@FindBy(xpath = "//div[contains(@class,'js-total-price')]")
	public WebElement totalCartPrice;

	/**
	 * Add to cart by url
	 * @param id of template
	 */
	public void addToCartById(String id) {
		getURL(CART_URL + "?add=" + id, driver);
		waitForCssLoadFromHeader(driver);
	}

	public void clickCheckoutNowButton() {
		clickOnElement(checkoutNowButton, "Checkout Now Button", driver);
		pageLoaderWait(driver, ELEMENT_TIMEOUT);
	}

	public void clearCart(WebDriver driver) {
		getURL(CLEAN_CART_URL, driver);
		waitForCssLoadFromHeader(driver);
		waitForElementVisible(clearCartIndex, driver);
	}

	public int getTotalCartPrice() {
		waitForElementVisible(totalCartPrice, driver);
		int price = getPrice(totalCartPrice, "totalCartPrice", driver);
		log.info("Total price on page = " + price);
		return price;
	}

	/**
	 * Get percent of cashback
	 * @return
	 */
	public int getPercentCachback() {
		waitForElementVisible(cashBackPercentElement, driver);
		int percent = getPrice(cashBackPercentElement, "cashBackPercentElement", driver);
		log.info("Percent of cachback = " + percent);
		return percent;
	}

	/**
	 * Add offer by id.
	 * @param id
	 */
	public void addOfferById(int id) {
		getURL(CART_URL + "?addOffer=" + id, driver);
		waitForCssLoadFromHeader(driver);
	}

}
