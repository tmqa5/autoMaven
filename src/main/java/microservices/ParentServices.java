package microservices;

import objects.User;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.jayway.jsonpath.JsonPath.read;
import static microservices.AuthServices.userApplicationAccessTokenHeaderUserName;
import static services.Constants.CLIENT_ID;
import static services.Constants.CLIENT_SECRET;
import static services.HttpSampler.Method.GET;
import static services.WebClientService.getBody;
import static services.WebClientService.sender;


public class ParentServices {

    public final static String USER_SCOPES =
            "authors " +
            "collections " +
            "event-subscriptions " +
            "events " +
            "notifications " +
            "orders " +
            "products " +
            "reviews " +
            "statistics " +
            "subscriptions " +
            "support-levels " +
            "transactions " +
            "uploads " +
            "user-auth-clients " +
            "users " +
            "templates "+
            "loyalty-users";

    public static String getUserDataUserName(User user, String path) {
        return getBody(sender(
                path,
                GET,
                userApplicationAccessTokenHeaderUserName(user)
        ));
    }

    public static Map getUserClientCredendials(User user, String grantType, String scope) {
        Map parametrs = new HashMap();
        parametrs.put("grant_type", grantType);
        parametrs.put("username", user.getEmail());
        parametrs.put("password", user.getPassword());
        parametrs.put("scope", scope);
        parametrs.put("client_id", CLIENT_ID);
        parametrs.put("client_secret", CLIENT_SECRET);
        return parametrs;
    }

    public static Map getUserClientCredendialsUserName(User user, String grantType, String scope) {
        Map parametrs = new HashMap();
        parametrs.put("grant_type", grantType);
        parametrs.put("username", user.getUserName());
        parametrs.put("password", user.getPassword());
        parametrs.put("scope", scope);
        parametrs.put("client_id", CLIENT_ID);
        parametrs.put("client_secret", CLIENT_SECRET);
        return parametrs;
    }

    public static LinkedHashMap getFirstElementFromApi(String data) {
        return new LinkedHashMap(read(data, "$.[0]"));
    }
}
