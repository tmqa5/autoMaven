package microservices;

import lombok.extern.log4j.Log4j;
import objects.User;
import org.json.JSONObject;
import services.HttpSampler;
import services.PropertyReader;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.*;
import java.util.Arrays;
import java.util.Map;

import static services.Constants.*;
import static services.HttpSampler.Method.POST;
import static services.WebClientService.*;

/**
 * Created by user
 */

@Log4j
public class AuthServices extends ParentServices {

    private static PropertyReader reader = new PropertyReader("properties/auth.properties");

    public static MultivaluedMap<String, Object> userApplicationAccessTokenHeader(User user) {
        return getApplicationAccessTokenHeader(AUTH_API_TOKEN_URL, getUserClientCredendials(user, "password", USER_SCOPES));
    }

    public static MultivaluedMap<String, Object> userApplicationAccessTokenHeaderUserName(User user) {
        return getApplicationAccessTokenHeader(AUTH_API_TOKEN_URL, getUserClientCredendialsUserName(user, "password", USER_SCOPES));
    }


    public static String generateTokenByUserName(User user, String grantType, String scope) {
        return getToken(AUTH_API_TOKEN_URL, getUserClientCredendialsUserName(user, grantType, scope));
    }

    /**
     * Get access new token for user.
     *
     * @return access token
     */
    public static String getNewTokenByUserName(User user) {
        return generateTokenByUserName(user, "password", USER_SCOPES);
    }

    public static MultivaluedMap<String, Object> getApplicationAccessTokenHeader(String host, Map parameters) {
        MultivaluedMap header = new MultivaluedHashMap();
        header.put(AUTHORIZATION_HEADER_KEY, Arrays.asList(getToken(host, parameters)));
        return header;
    }

    public static String getToken(String url, Map parameters) {
        Form form = new Form();

        fillFormByKeyOrUseDefault(form, parameters, GRANT_TYPE_KEY);
        fillFormByKeyOrUseDefault(form, parameters, CLIENT_ID_KEY);
        fillFormByKeyOrUseDefault(form, parameters, CLIENT_SECRET_KEY);
        fillFormByKeyOrUseDefault(form, parameters, USER_NAME_KEY);
        fillFormByKeyOrUseDefault(form, parameters, USER_PASSWORD_KEY);
        fillFormByKeyOrUseDefault(form, parameters, SCOPE_KEY);

        return getAccessToken(form, url);
    }

    private static Form fillFormByKeyOrUseDefault(Form form, Map<String, String> parameters, String key) {
        String param = parameters.get(key);
        return form.param(key, param != null ?  param : reader.getPropVal(key));
    }

    private static String getAccessToken(Form form, String url) {
        return getAuthData(form, url).getString("access_token");
    }

    /**
     * Retrieves auth info for specified user
     *
     * @param form OAuth params
     * @param url URL of Auth Service
     * @return JSON with access_token, user_id etc.
     */
    public static JSONObject getAuthData(Form form, String url) {
        Entity entity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        HttpSampler sampler = new HttpSampler(url, POST, entity, new MultivaluedHashMap<String, Object>());

        String response = getBody(send(sampler));
        log.info(response);
        return new JSONObject(response);
    }

}
