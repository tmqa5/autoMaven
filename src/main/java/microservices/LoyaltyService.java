package microservices;


import lombok.extern.log4j.Log4j;
import objects.Cart;
import objects.Level;
import objects.Range;
import objects.User;
import org.json.JSONArray;
import org.json.JSONObject;
import services.HttpClient;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static microservices.AuthServices.getNewTokenByUserName;
import static services.Constants.LOYALTY_LEVELS_URL;
import static services.Constants.conf;
import static services.WebClientService.getBody;
import static services.WebClientService.getResponse;

/**
 * Created by user
 */

@Log4j
public class LoyaltyService extends ParentServices{

	public static HttpClient httpClient = new HttpClient();
	public static String doubleFormat = "##0.00";
	public static String levelsApi = conf.loyaltyApiUrl()+"api/v1/loyalty-levels";
	/**
	 *
	 * @param cart object cart with total amount
	 * @return cashback percent
	 */
	public static int getCashBackPercent(Cart cart){
		String body = getBody(httpClient.get(LOYALTY_LEVELS_URL).getResponse());
		JSONArray arrayLevels = new JSONArray(body);
		int cashback = 0;

		JSONObject levelObject = arrayLevels.getJSONObject(0);
		JSONArray arrayRanges = levelObject.getJSONArray("ranges");
		for (Object range : arrayRanges){
			JSONObject rangeObject = new JSONObject(range.toString());
			int rangeFrom = rangeObject.getJSONObject("range").getInt("from");

			String rangeTo = rangeObject.getJSONObject("range").get("to").toString();
			int rangeToInt = !rangeTo.equals("null") ? parseInt(rangeTo) : 10000;

			if (verifyInterval(cart.getTotalAmount(), rangeFrom, rangeToInt)) {
				cashback = rangeObject.getInt("value");
				break;
			}
		}

		return cashback;
	}

	/**
	 *
	 * @param totalPrice total price of cart
	 * @param percent int percent, for example 7
	 * @return cashback value, for example 21.56
	 */
	public static double getCashbackAmount(int totalPrice, int percent) {
		DecimalFormat decimalFormat = new DecimalFormat(doubleFormat);
		double value = parseDouble(decimalFormat.format(totalPrice*percent/100F));
		log.info(percent+"% from "+totalPrice+ " = "+value);
		return value;
	}


	public  static boolean verifyInterval(int value, int from, int to){
		return value>=from && value<=to;
	}

	/**
	 * Method will return balance by user
	 * @param user
	 */
	public static double getBalanceByUser(User user){
		String body = getBody(Objects.requireNonNull(getResponse(conf.balanceApiUrl() +
				"transactions/balance/bonus?access_token=" + getNewTokenByUserName(user))));
		JSONObject object = new JSONObject(body);
		double balance = object.getDouble("balance");
		log.info("Balance - " + balance + "$");
		return balance;
	}

	/**
	 * Will return levels and ranges from api
	 */
	public static List<Level> getLevels(){
		List<Level> levels = new ArrayList<>();

		String body = getBody(Objects.requireNonNull(getResponse(levelsApi)));
		JSONArray array = new JSONArray(body);

		//Level.
		for (Object objectItem:array){
			Level level = new Level();
			JSONObject levelJson = new JSONObject(objectItem.toString());
			level.setId(levelJson.getString("id"));
			level.setOrders(levelJson.getInt("orders"));
			level.setName(levelJson.getString("name"));

			JSONArray rangesArray = levelJson.getJSONArray("ranges");

			//Range.
			List<Range> listRange = new ArrayList<>();
			for (Object objectRange:rangesArray){
				Range range = new Range();
				JSONObject rangeJson = new JSONObject(objectRange.toString());
				range.setFrom(rangeJson.getJSONObject("range").getInt("from"));

				if (!rangeJson.getJSONObject("range").get("to").equals(null)){
					range.setTo(rangeJson.getJSONObject("range").getInt("to"));
				}
				else {
					range.setTo(10000);
				}

				range.setValue(rangeJson.getInt("value"));
				listRange.add(range);
			}

			level.setRanges(listRange);
			levels.add(level);

		}

		return levels;
	}

	/**
	 * Will return actual Level
	 * @param orders count in cart
	 * @return
	 */
	public static Level getActualLevel(int orders){
		List<Level> levels = getLevels();

		Level actualLevel = null;
		for (Level level:levels){
			if (orders>=level.getOrders()){
				actualLevel = level;
				break;
			}
		}
		log.info("Actual level - " + actualLevel);
		return actualLevel;
	}

	/**
	 * will return actual range
	 * @param level
	 * @param price of cart
	 * @return
	 */
	public static Range getActualRange(Level level, int price){
		List<Range> ranges = level.getRanges();

		Range actualRange = null;
		for (Range range:ranges){
			if (verifyInterval(price, range.getFrom(), range.getTo())){
				actualRange = range;
				break;
			}
		}
		log.info("Actual range - " + actualRange);
		return actualRange;
	}

	/**
	 * Return actual percent of cashback
	 * @param orders
	 * @param price
	 */
	public static int getActualPercent(int orders, int price){
		return getActualRange(getActualLevel(orders), price).getValue();
	}
}
