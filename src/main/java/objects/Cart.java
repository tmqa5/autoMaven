package objects;

import lombok.Data;

import java.util.List;

/**
 * Created by user
 */
@Data
public class Cart {
	private String id;
	private int totalCount;
	private int totalAmount;
	private int subTotalAmount;
	private List<Item> items;
}
