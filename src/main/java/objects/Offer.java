package objects;

import lombok.Data;
import services.ObjectService;
import services.PropertyReader;

/**
 * Created by user
 */
@Data
public class Offer {
	private int id;

	public Offer(String fileLocation) {
		super();
		PropertyReader propertyReader = new PropertyReader(fileLocation);
		this.id = ObjectService.parser(propertyReader.getValue("id"));

	}
}
