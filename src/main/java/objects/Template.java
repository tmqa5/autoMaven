package objects;

import lombok.Data;
import services.PropertyReader;

/**
 * Created by user
 */
@Data
public class Template {
	private String templateName;
	private Integer templatePrice;
	private String templateID;
	private String templateLicence;
	private Boolean priceDiscount;
	private Integer originPrice;

	public Template(String fileLocation) {
		PropertyReader propertyReader = new PropertyReader(fileLocation);
		this.templateName = propertyReader.getValue("templateName");
		this.templateID = propertyReader.getValue("templateID");
		this.templateLicence = propertyReader.getValue("templateLicence");
		if (propertyReader.getValue( "templatePrice")!=null)this.templatePrice = Integer.valueOf(propertyReader.getValue( "templatePrice"));
	}

}


