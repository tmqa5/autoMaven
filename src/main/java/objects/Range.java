package objects;

import lombok.Data;

/**
 * Created by user
 */
@Data
public class Range {
	int from;
	int to;
	int value;
}
