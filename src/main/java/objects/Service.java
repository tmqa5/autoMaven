package objects;

import lombok.Data;

/**
 * Created by user
 */
@Data
public class Service {
	private int id;
	private String link;
	private int price;
	private int finalPrice;
}
