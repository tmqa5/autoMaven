package objects;

import lombok.Data;

import java.util.List;

/**
 * Created by user
 */
@Data
public class Level {
	String id;
	int orders;
	String name;
	List<Range> ranges;
}
