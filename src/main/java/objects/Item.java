package objects;

import lombok.Data;

import java.util.List;

/**
 * Created by user
 */
@Data
public class Item {
	private int id;
	private String link;
	private int price;
	private int finalPrice;
	private List<Service> services;
}
