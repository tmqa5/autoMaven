package objects;

import lombok.Data;
import lombok.NoArgsConstructor;
import services.PropertyReader;

/**
 * Created by user
 */
@Data
@NoArgsConstructor
public class User {
	private String id;
	private String userName;
	private String password;
	private String email;

	public User(String fileLocation) {
		PropertyReader propertyReader = new PropertyReader(fileLocation);
		this.userName = propertyReader.getValue("userName");
		this.password = propertyReader.getValue("password");
		this.id = propertyReader.getValue("id");

		//Generate random email.
		String emailDef = propertyReader.getValue("email");
		if (emailDef != null) {
			this.email = emailDef.substring(0, emailDef.indexOf("@")) + "-" +
				String.valueOf(System.currentTimeMillis()) + emailDef.substring(emailDef.indexOf("@"));
		}
	}
}
