package objects;

import lombok.Data;
import services.PropertyReader;

/**
 * Created by user
 */
@Data
public class Order {
	private String id;
	private String merchantId;
	private String status;
	private String pageStatus;
	private String description;
	private String reason;
	private String reasonText;
	private String action;
	private Integer price;
	private String state;
	private String cabinetState;
	private String billingState;


	public Order(String fileLocation){
		PropertyReader propertyReader = new PropertyReader(fileLocation);
		this.id = propertyReader.getValue( "id");
		this.cabinetState = propertyReader.getValue("cabinetState");
		this.status = propertyReader.getValue( "status");
		this.pageStatus = propertyReader.getValue( "pageStatus");
		this.description = propertyReader.getValue( "description");
		this.reason = propertyReader.getValue( "reason");
		this.reasonText = propertyReader.getValue( "reasonText");
		this.action = propertyReader.getValue( "action");
		this.state = propertyReader.getValue( "state");
		this.merchantId = propertyReader.getValue( "merchantId");
		this.billingState = propertyReader.getValue( "billingState");

	}

	public Order(){}

}

