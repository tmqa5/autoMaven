package services;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import lombok.extern.log4j.Log4j;
import objects.User;

import static microservices.ParentServices.getFirstElementFromApi;
import static microservices.ParentServices.getUserDataUserName;
import static services.Constants.ORDERS_API;

/**
 * Created by user
 */
@Log4j
public class OrderService {


	/**
	 * Get last order id
	 * @param user
	 * @return
	 */
	public static String getLastOrderIdUserName(User user) {
		try {
			return JsonPath.read(getFirstElementFromApi(getUserOrdersUserName(user)), "id");
		} catch (PathNotFoundException e) {
			log.warn("getLastOrderId empty");
			return null;
		}
	}

	/**
	 * Get all orders from api
	 * @param user
	 * @return
	 */
	public static String getUserOrdersUserName(User user) {
		return getUserDataUserName(user, ORDERS_API + "orders?sort=date");
	}

}
