package services;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

/**
 * Created by user
 */
@Log4j
public class PressKeyService {

	public static void pressArrowRight(WebElement element){
		element.sendKeys(Keys.ARROW_RIGHT);
		log.info("Press \"Arrow Right\" key.");
	}

	public static void pressBackSpace(WebElement element){
		element.sendKeys(Keys.BACK_SPACE);
		log.info("Press \"Back Space\" key.");
	}
}
