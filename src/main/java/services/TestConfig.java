package services;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:${conf_file}.properties", "classpath:general.properties"})
public interface TestConfig extends Config {

	@Config.DefaultValue("https://www.templatemonsterdev.com/")
	String url();

	@Config.DefaultValue("https://api.templatemonsterdev.com/oauth/")
	String authApiUrl();

	@Config.DefaultValue("VKLpjMvKtDBCLVj9fzfESTGyjJSTTzLejfytDUt8")
	String clientId();

	@Config.DefaultValue("6DSp98jxxUefQPOYwSGB1arhO5jiKVfa8Mqsdabb")
	String clientSecret();

	@Config.DefaultValue("http://service-loyalty-programs.templatemonsterdev.com/")
	String loyaltyApiUrl();

	@Config.DefaultValue("https://api.templatemonsterdev.com/orders/v2/")
	String orderApi();

	@Config.DefaultValue("http://service-balances.templatemonsterdev.com/api/v2/")
	String balanceApiUrl();

	@Config.DefaultValue("https://account.templatemonsterdev.com/")
	String cabinet();

}
