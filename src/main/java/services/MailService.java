package services;

import lombok.extern.log4j.Log4j;
import org.jsoup.Jsoup;

import javax.mail.*;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.Properties;

import static services.Constants.*;
import static services.ReportService.assertTrue;

/**
 * Created by user
 */
@Log4j
public class MailService {
	/**
	 * @param theme part of subject
	 * @param part of content
	 */
	public static boolean verifyLetterContent(String theme, String part){
		try {
			Folder folder = getInbox();
			folder.open(Folder.READ_ONLY);

			Message[] messages = folder.getMessages();
			label:for (Message message : messages) {
				try {
					if (message.getSubject().contains(theme) && getLetterContent(message).contains(part)) {
						log.info("Letter is found!");
						folder.close(true);
						return true;
					}
				}
				catch (MessagingException e){
					continue label;
				}
			}
			folder.close(true);
		}
		catch (MessagingException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Get all letters
	 * @return
	 */
	public static Folder getInbox(){
		Properties properties  = new Properties();
		properties.setProperty("mail.store.protocol", MAIL_PROTOCOL);
		Folder inbox = null;
		try {
			Session session = Session.getInstance(properties, null);
			Store store = session.getStore();
			store.connect(MAIL_SERVER, USER_EMAIL, USER_PASS);
			inbox = store.getFolder("INBOX");
		}
		catch (MessagingException e ){
			e.printStackTrace();
		}
		if (inbox==null){
			assertTrue(false, "Something going wrong with email.");
		}
		return inbox;
	}

	/**
	 * Method will return content from letter
	 * @param message
	 * @return
	 */
	public static String getLetterContent(Message message){
		String result = "";
		try {
			if (message.isMimeType("text/plain")) {
				result = message.getContent().toString();
			}
			else if (message.isMimeType("multipart/*")) {
				MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
				result = getTextFromMimeMultipart(mimeMultipart);
			}
		} catch (MessagingException | IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * @param mimeMultipart
	 * @return content from multipart letter.
	 */
	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart)  throws MessagingException, IOException{
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			}
			else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + Jsoup.parse(html).text();
			}
			else if (bodyPart.getContent() instanceof MimeMultipart){
				result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
			}
		}
		return result;
	}
}
