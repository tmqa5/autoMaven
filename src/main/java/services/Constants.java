package services;

import org.openqa.selenium.Dimension;

import static base.BaseTestCase.initTestConfig;

/**
 * Created by user
 */
public class Constants {

	public static TestConfig conf = initTestConfig();
	public static final String AUTH_API_TOKEN_URL = conf.authApiUrl()+"token";
	public static final String CLIENT_ID = conf.clientId();
	public static final String CLIENT_SECRET = conf.clientSecret();
	public static final String LOYALTY_LEVELS_URL = conf.loyaltyApiUrl()+"api/v1/loyalty-levels";
	public static final String AUTHORIZATION_HEADER_KEY = "Authorization";
	public static final String GRANT_TYPE_KEY = "grant_type";
	public static final String CLIENT_ID_KEY = "client_id";
	public static final String CLIENT_SECRET_KEY = "client_secret";
	public static final String USER_NAME_KEY = "username";
	public static final String USER_PASSWORD_KEY = "password";
	public static final String SCOPE_KEY = "scope";
	public static final String ORDERS_API = conf.orderApi();
	public static final String CLEAN_CART_URL ="cart.php?rid=clearcart";
	public static final String SALES_BILLING_URL ="sales/billing/";
	public static final String CABINET = conf.cabinet();
	public static  String LOCALE = System.getProperty("locale", "");
	public static final String LOCALE_FOR_CABINET_URL = LOCALE.isEmpty()?"en":LOCALE;
	public static final String ACCOUNT_LOYALTY_PHP = CABINET + "?lang="+LOCALE_FOR_CABINET_URL + "#/loyalty";
	public static final String CONTAINS_ORDERS_URL = "orders";
	public static final String CART_URL = "cart.php";

	//***************************** TIMEOUTS *************************************************
	public static final int PAGE_TIMEOUT = 60;
	public static final int ELEMENT_TIMEOUT = 20;
	public static final int ELEMENT_SMALL_TIMEOUT = 10;

	//***************************** DIMENSIONS *************************************************
	public static final Dimension FULL_HD = new Dimension(1920, 1080);

	//***************************** MAIL PROPS *************************************************
	public static final String MAIL_SERVER = "mail.devoffice.com";
	public static final String MAIL_PROTOCOL = "pop3";
	public static final String USER_EMAIL = "autotest@devoffice.com";
	public static final String USER_PASS = "RiLi9ohx";
}
