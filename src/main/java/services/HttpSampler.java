package services;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by user
 */
public class HttpSampler {
    private String url;
    public enum Method {POST, GET, PUT};
    private Method method;
    Entity entity;
    MultivaluedMap<String, Object> header;

    public HttpSampler(String url, Method method, Entity entity, MultivaluedMap<String, Object> header) {
        this.url = url;
        this.method = method;
        this.entity = entity;
        this.header = header;
    }

    public String getUrl() {
        return url;
    }

    public String getMethodName() {
        return method.name();
    }

    public Entity getEntity() {
        return entity;
    }

    public MultivaluedMap<String, Object> getHeader() {
        return header;
    }
}
