package services;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import static services.ObjectService.trimer;
import static services.PressKeyService.pressArrowRight;
import static services.PressKeyService.pressBackSpace;
import static services.ReportService.assertTrue;
import static services.UrlService.scrollDown;
import static services.UrlService.stopLoad;
import static services.WaiterService.sleep;
import static services.WaiterService.waitForElementVisible;

/**
 * Created by user by user
 */
@Log4j
public class WebElementService {

	/**
	 * Move mouse to coordinate
	 * @param x
	 * @param y
	 * @param driver
	 */
	public static void moveToCoordinate(int x, int y, WebDriver driver) {
		Actions actions = new Actions(driver);
		actions.moveByOffset(x, y).build().perform();
		log.info("Move to coordinate "+x+"x"+y);
	}

	/**
	 * Method wait that element will be clickable and doing click
	 * @param element
	 * @param elementName
	 * @param driver
	 */
	public static void clickOnElement(WebElement element, String elementName, WebDriver driver) {
		waitForElementVisible(element, driver);
		try {
			element.click();
			log.info("Click on \"" + elementName + "\".");
		}
		catch (NoSuchElementException e) {
			assertTrue(false, "\"" + elementName + "\" was not found on page after timeout.");
		}
		catch (ElementNotVisibleException e) {
			log.error("ElementNotVisibleException");
			clickHack(element, elementName, driver);
		}
		catch (TimeoutException e) {
			stopLoad(driver);
		}
		catch (StaleElementReferenceException e) {
			log.warn("StaleElementReferenceException.");
			log.info("Click on \"" + elementName + "\".");
			element.click();
		}
		catch (WebDriverException e) {
			log.error("WebDriverException" + e);
			clickHack(element, elementName, driver);
		}
	}

	/**
	 * Click hack - scroll down page if element is invisible (in bottom page)
	 * @param element
	 * @param elementName
	 * @param driver
	 */
	private static void clickHack(WebElement element, String elementName, WebDriver driver) {
		boolean flag = true;
		int attempt = 0;

		while (flag && attempt < 5) {
			attempt++;
			try {
				log.info("\"" + elementName + "\" is hide by another element, move down...");
				scrollDown(driver);
				moveToCoordinate(0, 0, driver);
				element.click();
				log.info("Click on \"" + elementName + "\".");
				flag = false;
			} catch (WebDriverException ignored) {
			}
		}
	}

	/**
	 * Method will return text from element.
	 * @param element
	 * @param elementName
	 * @return String (text from element)
	 */
	public static String getElementText(WebElement element, String elementName) {

		try {
			String text = element.getText();
			log.info("\"" + elementName + "\" content on page  - \"" + trimer(text) + "\".");
			return text;
		}
		catch (NoSuchElementException | ElementNotVisibleException e) {
			assertTrue(false, "\"" + elementName + "\" was not found on page after timeout.");
			throw new CustomException(e.toString());
		}
	}

	/**
	 * Input text to field
	 * @param element of field
	 * @param elementName
	 * @param inputText
	 */
	public static void inputText(WebElement element, String elementName, String inputText) {
		try {
			element.sendKeys(inputText);
			log.info("\"" + elementName + "\" input text: \"" + inputText + "\".");
		}
		catch (NoSuchElementException e) {
			assertTrue(false, "\"" + elementName + "\" was not found on page after timeout.");
		}
		catch (ElementNotVisibleException e) {
			assertTrue(false, "\"" + elementName + "\" was not visible.");
		}

	}

	/**
	 * Select drop box by text
	 * @param element
	 * @param text
	 */
	public static void selectDropBoxByText(WebElement element, String text) {
		try {
			Select select = new Select(element);
			select.selectByVisibleText(text);
			log.info("Select \"" + text + "\".");
		}
		catch (NoSuchElementException e) {
			assertTrue(false, "\"" + text + "\" is missing!");
		}
	}

	/**
	 * Mouse move to element
	 * @param element
	 * @param elementName
	 * @param driver
	 */
	public static void moveToElement(WebElement element, String elementName, WebDriver driver) {
		try {
			waitForElementVisible(element, driver);
			Actions actions = new Actions(driver);
			actions.moveToElement(element).build().perform();
			sleep(1);
			log.info("\"" + elementName + "\" is active.");
		}
		catch (NoSuchElementException e) {
			assertTrue(false, "\"" + elementName + "\" not found.");
		}
		catch (ElementNotVisibleException e) {
			assertTrue(false, "\"" + elementName + "\" was not visible.");
		}
	}

	/**
	 * Verify that element is displayed
	 * @param element
	 * @param elementName
	 * @return
	 */
	public static boolean elementIsDisplayed(WebElement element, String elementName) {
		try {
			if (element != null && element.isDisplayed()) {
				return true;
			}
			else {
				log.info("\"" + elementName + "\" is not displayed.");
				return false;
			}
		}
		catch (NoSuchElementException e) {
			log.info("\"" + elementName + "\" is NOT displayed.");
			return false;
		}
		catch (ElementNotVisibleException e) {
			assertTrue(false, "\"" + elementName + "\" was not visible.");
			return false;
		}
		catch (StaleElementReferenceException e) {
			return false;
		}
	}

	/**
	 * Method cleared the field (BackSpace)
	 * @param element input on page
	 * @param driver Webdriver
	 * @param clickArrowRightAtField huck for checkout bonus form - at every iteration before backspace click
	 */
	public static void clearFieldManual(WebElement element, WebDriver driver, boolean clickArrowRightAtField) {
		int countChar = getElementAttribute(element, element.toString(), "value").length();
		int i = 0;
		while (i < countChar) {
			i++;
			clickOnElement(element, "element", driver);
			if (clickArrowRightAtField)
				pressArrowRight(element);
			pressBackSpace(element);
		}
	}

	/**
	 * Return attribute by name
	 * @param element
	 * @param elementName
	 * @param attribute
	 * @return
	 */
	public static String getElementAttribute(WebElement element, String elementName, String attribute) {
		String attributeValue = "";
		try {
			//Get value.
			if (element.getAttribute(attribute) != null) {
				attributeValue = element.getAttribute(attribute);
			}
			else {
				assertTrue(false, "Attribute \"" + attribute + "\" not present.");
			}
			log.info(attribute + " \"" + elementName + "\" = \"" + attributeValue + "\".");
		}
		catch (NoSuchElementException e) {
			assertTrue(false, "\"" + elementName + "\" was not found on page after timeout.");
		}
		catch (ElementNotVisibleException e) {
			assertTrue(false, "\"" + elementName + "\" was not visible.");
		}
		return attributeValue;
	}

}
