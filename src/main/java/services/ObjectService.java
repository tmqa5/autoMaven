package services;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.util.regex.Pattern.compile;
import static services.WaiterService.waitForElementVisible;
import static services.WebElementService.getElementText;

/**
 * Created by user
 */
@Log4j
public class ObjectService {
	/**
	 * Method will replace several spaces for one
	 * @param text - original text
	 * @return
	 */
	public static String trimer (String text){
		return text.replaceAll("\\s+", " ");
	}

	/**
	 * Method will return Integer fom text
	 * @param text
	 */
	public static Integer parser(String text){
		try {
			return (text!=null && !text.isEmpty()) ? Integer.parseInt(text) : null;
		}
		catch (NumberFormatException e){
			throw new NumberFormatException("NumberFormatException");
		}
	}

	/**
	 * @param element
	 * @return double with .xx
	 */
	public static double getPriceDouble(WebElement element){
		String doubleValue = getElementText(element, "element").replaceAll("\\D", "");
		String withDelimeter = doubleValue.substring(0, doubleValue.length()-2) +
				"."+doubleValue.substring(doubleValue.length()-2, doubleValue.length());
		return parseDouble(withDelimeter);
	}

	/**
	 * Method will return price (different options on the page)
	 * @param element
	 * @param elementName
	 * @param driver
	 * @return
	 */
	public static Integer getPrice(WebElement element, String elementName, WebDriver driver) {

		try {
			waitForElementVisible(element, driver);

			Pattern pattern = compile("\\d{1,4}(,\\d{1,3})?");
			Matcher matcher = pattern.matcher(getElementText(element, "element price"));
			List<String> arrayPrice = new ArrayList<>();

			while (matcher.find()){
				arrayPrice.add(matcher.group());
			}

			if(arrayPrice.size()==1)
			{
				log.info("Price - " + "\"" + arrayPrice.get(0) + "\"");
				return parseInt(arrayPrice.get(0).replace(",",""));
			}
			else if (arrayPrice.size()==2)
			{
				int priceOne = parseInt(arrayPrice.get(0).replace(",",""));
				int priceTwo = parseInt(arrayPrice.get(1).replace(",",""));
				int priceActual;
				priceActual=priceOne>priceTwo?priceTwo:priceOne;
				log.info("Price - " + "\"" + priceActual + "\"");
				return priceActual;
			}
			else if(arrayPrice.size()==0) {
				throw new CustomException("Any price on "+elementName);
			}
			else {
				return parseInt(arrayPrice.get(arrayPrice.size()-1).replace(",",""));
			}

		}
		catch (NoSuchElementException | ElementNotVisibleException e){
			throw new CustomException("Exception: "+e);
		}
	}

	/**
	 * Return random value from range [min and max]
	 * @param min
	 * @param max
	 * @return
	 */
	public static int random(int min, int max)
	{
		max -= min;
		return (int) (Math.random() * ++max) + min;
	}
}
