package services;

import objects.User;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

/**
 * Created by user
 */
public class WebClientService {

	/**
	 * Perform request
	 *
	 * @param method HTTP method of request
	 * @param link URL of request
	 * @param body Body of request. Applicable only for POST and PUT methods
	 * @param user User for Basic auth
	 * @param headers Headers of request
	 * @return Response data
	 */
	public static Response getResponse(String method, String link, String body, User user, MultivaluedMap<String, Object> headers) {
		String contentMediaType = (String) headers.getFirst(HttpHeaders.CONTENT_TYPE);
		Invocation.Builder target = getHttpClient().target(link).request();

		target.headers(headers);

		if (user != null) {
			String encoding = new String(Base64.encodeBase64(("" + user.getUserName() + ":" + user.getPassword() + "").getBytes()));
			target.header(HttpHeaders.AUTHORIZATION, "Basic " + encoding);
		}

		switch (method) {
			case "POST":
				return target.post(Entity.entity(body, contentMediaType));
			case "PUT":
				return target.put(Entity.entity(body, contentMediaType));
			case "GET":
				return target.get();
			case "DELETE":
				return target.delete();
			default:
				throw new CustomException("Unsupported HTTP method - " + method);
		}
	}

	/**
	 * Perform GET request with auth
	 *
	 * @param link URL to perform request
	 * @param user User to auth
	 * @return Response of GET request
	 */
	public static Response getResponse(String link, User user) {
		return getResponse("GET", link, user);
	}

	/**
	 * Perform GET request with auth
	 *
	 * @param method HTTP method
	 * @param link URL to perform request
	 * @param user User to auth
	 * @return Response of GET request
	 */
	public static Response getResponse(String method, String link, User user) {
		return getResponse(method, link, "", user, MediaType.APPLICATION_JSON);
	}

	public static Response getResponse(String method, String link, String body, User user, String contentMediaType) {
		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
		headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
		headers.add(HttpHeaders.CONTENT_TYPE, contentMediaType);

		return getResponse(method, link, body, user, headers);
	}

	/**
	 * Get HTTP client that do not validate HTTPS certificates.
	 *
	 * @return HTTP client to perform requests
	 */
	public static Client getHttpClient() {
		try {
			TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			}
			};

			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new SecureRandom());
			return ClientBuilder.newBuilder().sslContext(sc).hostnameVerifier((s, sslSession) -> true).build();
		} catch (Exception e) {
			throw new RuntimeException("Failed to init HTTP client. " + e.getMessage());
		}
	}

	/**
	 * @param response
	 * @return String body (json)
	 */
	public static String getBody(Response response){
		return response.readEntity(String.class);
	}

	public static Response sender(String url, HttpSampler.Method method, MultivaluedMap<String, Object> header) {
		return send(new HttpSampler(url, method, null, header));
	}

	public static Response sender(String url, String body) {
		return send(new HttpSampler(url, HttpSampler.Method.POST, Entity.entity(body, MediaType.APPLICATION_FORM_URLENCODED_TYPE), new MultivaluedHashMap<String, Object>()));
	}

	public static Response sender(String url, String body, HttpSampler.Method method) {
		return send(new HttpSampler(url, method, Entity.entity(body, MediaType.APPLICATION_FORM_URLENCODED_TYPE), new MultivaluedHashMap<String, Object>()));
	}

	public static Response sender(String url, String body, HttpSampler.Method method, MultivaluedMap<String, Object> header) {
		return send(new HttpSampler(url, method, Entity.entity(body, MediaType.APPLICATION_FORM_URLENCODED_TYPE), header));
	}

	public static Response sender(String url, Form form, HttpSampler.Method method, MultivaluedMap<String, Object> header) {
		return send(new HttpSampler(url, method, Entity.form(form), header));
	}

	public static Response send(HttpSampler sampler){
		Client client = getHttpClient();

		return client.target(sampler.getUrl())
				.request()
				.headers(sampler.getHeader())
				.build(sampler.getMethodName(), sampler.getEntity()).invoke();
	}

	/**
	 * @param link
	 * @return HttpResponse
	 */
	public static HttpResponse getResponse(String link){
		try {
			SSLContextBuilder builder = new SSLContextBuilder();
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build(),SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);


			Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
					.register("http", new PlainConnectionSocketFactory())
					.register("https", sslsf)
					.build();


			PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);
			cm.setMaxTotal(2000);

			CloseableHttpClient httpClient = HttpClients.custom()
					.setSSLSocketFactory(sslsf)
					.setConnectionManager(cm)
					.build();

			HttpResponse response = httpClient.execute(new HttpGet(link));
			return response;
		}
		catch (Exception e){
			ReportService.assertFalse(true, "Catch "+e);
			return null;
		}
	}

	/**
	 * @param response
	 * @return body
	 */
	public static String getBody(HttpResponse response){
		try {
			return EntityUtils.toString(response.getEntity(), "UTF-8");
		}
		catch (IOException e){
			throw new CustomException("Catch "+e);
		}
	}

}
