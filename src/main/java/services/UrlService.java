package services;


import lombok.extern.log4j.Log4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.List;

import static services.Constants.FULL_HD;
import static services.Constants.PAGE_TIMEOUT;
import static services.Constants.conf;
import static services.ReportService.assertTrue;
import static services.WaiterService.pageLoaderWait;
import static services.WaiterService.sleep;
import static services.WebElementService.moveToCoordinate;

/**
 * Created by user
 */
@Log4j
public class UrlService {

    /**
     * @param url navigate to url (main env + url, for example - /templates.php)
     * @param driver
     */
    public static void getURL(String url, WebDriver driver) {
        log.info("Navigate to \""+conf.url()+url+"\"...");
        driver.get(conf.url()+url);
        pageLoaderWait(driver, PAGE_TIMEOUT);
        log.info("Navigate to \"" + conf.url()+url + "\" finished.");
    }

    /**
     * @param url navigate to url fully, for example - https://www.google.com/
     * @param driver
     */
    public static void getDirectlyURL(String url, WebDriver driver) {
        driver.getCurrentUrl();
        log.info("Navigate to \""+url+"\"...");
        driver.get(url);
		pageLoaderWait(driver, PAGE_TIMEOUT);
        log.info("Navigate to \"" + url + "\" finished.");
        moveToCoordinate(0,0,driver);
    }


    /**
     * Reload current page
     * @param driver
     */
    public  static void refreshPage(WebDriver driver){
        try {
            driver.navigate().refresh();
            log.info("Page was refreshed.");
        }
        catch (WebDriverException e){
            stopLoad(driver);
        }
    }

    /**
     * @param frameIndex in which need to switch, for example - 0
     * @param driver
     */
    public static void switchToFrame(int frameIndex, WebDriver driver){
        List<WebElement> frames = driver.findElements(By.tagName("frame"));
        int attemptCounter = 0;
        while (frames.size()<=frameIndex){
            frames = driver.findElements(By.tagName("frame"));
            sleep(1);
            attemptCounter++;
            if (attemptCounter == 10){
                assertTrue(false,"Invalid index of frame");
            }
        }

        driver.switchTo().frame(frames.get(frameIndex));
        log.info("Switch to "+frameIndex+"-index frame.");

    }

    /**
     * Scroll down
     * @param driver
     */
    public static void scrollDown(WebDriver driver){
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("window.scrollBy(0,document.body.scrollHeight)", "");
    }


    /**
     * Stop loading page (key Escape)
     * @param driver
     */
    public static void stopLoad(WebDriver driver){
        driver.findElement(By.tagName("body")).sendKeys(Keys.ESCAPE);
        log.info("Timeout on loading page \""+driver.getCurrentUrl()+"\".");
    }

    /**
     * Switch to last window
     * @param driver
     */
    public static void switchToLastWindowClose(WebDriver driver){
        try {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver driver) {
                    return (driver.getWindowHandles().size() > 1) ;
                }
            });
            String last = "";
            Iterator<String> iterator = driver.getWindowHandles().iterator();
            while (iterator.hasNext()){
                last=iterator.next();
            }
            for (String win:driver.getWindowHandles()){
                if (win.equals(last)) {
                    driver.switchTo().window(win);
                }else driver.close();
            }
            log.info("Switch to another window.");
        }
        catch (TimeoutException e){
            assertTrue(false, "You have only one window.");
        }
        driver.manage().window().setSize(FULL_HD);
        log.info("Maximize window.");
    }

	/**
	 * Switch to frame by id
	 * @param frameId
	 * @param driver
	 */
	public static void switchToFrame(String frameId, WebDriver driver){
        int attempt=0;
        boolean flag = true;
        while (flag && attempt<10){
            attempt++;
            try {
                driver.switchTo().frame(frameId);
                log.info("Switched to \""+frameId+"\" frame.");
                flag = false;
            }
            catch (NoSuchFrameException e){
                log.warn("NoSuchFrameException "+frameId);
            }
            catch (UnhandledAlertException e){
                skipModalWindow(driver);
            }
        }
    }

	/**
	 * Accept modal window.
	 * @param driver
	 */
	public static void skipModalWindow(WebDriver driver){
        try {
            Alert alt=driver.switchTo().alert();
            alt.accept();
            log.info("Skip modal window.");
        }
        catch (NoAlertPresentException e){
            log.warn("Catch NoAlertPresentException.");
        }
    }

}
