package services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.log4j.Log4j;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static services.WebClientService.getHttpClient;

/**
 * Created by user
 */

@Log4j
public class HttpClient {

    private String method;
    private String url;
    private MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
    private MultivaluedMap<String, String> parameters = new MultivaluedHashMap<>();

    public HttpClient() {
    }

    /**
     * Convert Response to List of Reviews
     *
     * @param response Response with entity that contains list of reviews
     * @return ArrayList of entities
     * @deprecated
     */
    public static <T> List<T> convertResponseToList(Response response, Class<T> clazz) {
        List<T> reviewsList = new ArrayList<>();
        String responseAsString = response.readEntity(String.class);

        try {
            JSONArray reviewsJsonArray = new JSONArray(responseAsString);
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

            for (int i = 0; i < reviewsJsonArray.length(); i++) {
                JSONObject reviewObject = (JSONObject) reviewsJsonArray.get(i);
                reviewsList.add(mapper.readValue(reviewObject.toString(), clazz));
            }
        } catch (Exception e) {
            log.error("Error while processing response");
            throw new RuntimeException(responseAsString, e);
        }
        return reviewsList;
    }

    public HttpClient get(String url) {
        this.url = url;
        this.method = HttpMethod.GET;
        this.parameters = null; // Entity must be null for http method GET.
        return this;
    }

    public Response getResponse() {
        try {
            headers.putAll(getDefaultHeaders());
            log.info("Perform " + method + " request to " + url);
            return getHttpClient().target(url).
                    request().
                    headers(headers).
                    build(method, Entity.entity(parameters, (String) headers.getFirst(CONTENT_TYPE))).
                    invoke();
        } finally {
            if (parameters != null) {
                parameters.clear();
            }
            headers.clear();
        }
    }

    private MultivaluedMap<String, Object> getDefaultHeaders() {
        MultivaluedMap<String, Object> defaultHeaders = new MultivaluedHashMap<>();
        defaultHeaders.add(ACCEPT, APPLICATION_JSON);
        defaultHeaders.add(CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED);
        return defaultHeaders;
    }


}
