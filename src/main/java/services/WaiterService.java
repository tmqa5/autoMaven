package services;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.HeaderPage;

import static org.openqa.selenium.support.PageFactory.initElements;
import static services.Constants.ELEMENT_TIMEOUT;
import static services.Constants.PAGE_TIMEOUT;
import static services.MailService.verifyLetterContent;
import static services.ReportService.assertTrue;
import static services.UrlService.stopLoad;

/**
 * @author Created by user
 */
@Log4j
public class WaiterService {

    /**
     * Method will wait load page (page load complete)
     * @param driver
     * @param timeout
     */
    public static void pageLoaderWait(WebDriver driver, int timeout) {
        log.info("Wait " + timeout + " seconds for page to load...");
        try {
            new WebDriverWait(driver, timeout)
                    .withMessage("Page was not loaded after timeout.")
                    .until((ExpectedCondition<Boolean>) d ->
                            ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete"));
        }
        catch (WebDriverException e) {
            log.warn(e);
        }
    }

    /**
     * @param element which need to wait
     * @param driver
     */
    public static void waitForElementVisible(WebElement element, WebDriver driver) {
        try {
            WebDriverWait wait = new WebDriverWait(driver,Constants.PAGE_TIMEOUT);
            wait.until(ExpectedConditions.visibilityOf(element));
        }
        catch (TimeoutException e){
            assertTrue(false, "Element: \"" + element + "\" is not presents.");
        }
        catch (StaleElementReferenceException e){
			log.warn(("Element: \"" + element + "\" is not found in the cache."));
        }
    }

    /**
     * @param seconds - pause in a test
     */
    public static void sleep(int seconds){
        try {
            Thread.sleep(seconds*1000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param url - contains url which need to wait (default timeout)
     * @param driver
     */
    public static void waitPageLoader(String url, WebDriver driver) {
        waitPageLoader(url, PAGE_TIMEOUT, driver);
    }

    /**
     * @param url - contains url which need to wait
     * @param seconds - timeout
     * @param driver
     */
    public static void waitPageLoader(String url, int seconds, WebDriver driver) {
        try {
            log.info("Waiting for \"" + url + "\" page...");
			(new WebDriverWait(driver, seconds))
					.withMessage("Url is not appear on element after timeout.")
					.until(((ExpectedCondition<Boolean>) d -> driver.getCurrentUrl().contains(url)));
        }
        catch (TimeoutException e) {
            stopLoad(driver);
        }
	}

    /**
     * @param element which need to wait
     * @param driver
     */
    public static void waitForElementClickable(WebElement element, WebDriver driver) {
        try {
            WebDriverWait wait = new WebDriverWait(driver,ELEMENT_TIMEOUT);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        }
        catch (TimeoutException e){
            assertTrue(false, "Element: \"" + element + "\" is not clickable.");
        }
        catch (StaleElementReferenceException e){
            log.warn(("Element: \"" + element + "\" is not found in the cache."));
        }
    }

    /** The method waiting when locator is appear
     * @param locator should be present
     * @param driver WebDriver
     */
    public static void waitForElementIsPresent(By locator, WebDriver driver) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, ELEMENT_TIMEOUT);
            wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        }
        catch (TimeoutException e){
            e.printStackTrace();
        }
    }


    /**
     * Wait for cookie by cookie name
     * @param cookieName
     * @param driver
     */
    public static void waitForCookie(String cookieName, WebDriver driver){
        try {
            log.info("Waiting for \"" + cookieName + "\" cookie...");
            (new WebDriverWait(driver, Constants.ELEMENT_TIMEOUT))
                    .withMessage("Cookie is not appear after timeout.")
                    .until(((ExpectedCondition<Boolean>) d -> (
                            driver.manage().getCookieNamed(cookieName) != null &&
                            driver.manage().getCookieNamed(cookieName).getValue() == null)));
        }
        catch (TimeoutException e) {
            stopLoad(driver);
        }
    }

    /**
     * Hack waiter - waiting slowly element (collection)
     * @param driver
     */
    public static void waitForCssLoadFromHeader(WebDriver driver){
        HeaderPage headerPage = initElements(driver, HeaderPage.class);
        waitForElementClickable(headerPage.collection, driver);
    }

    /**
     * Wait for element without break of test.
     * @param element
     * @param delay
     * @param driver
     */
    public static void waitForElementVisible(WebElement element, int delay, WebDriver driver) {

        try {
            WebDriverWait wait = new WebDriverWait(driver,delay);
            wait.until(ExpectedConditions.visibilityOf(element));
        }
        catch (TimeoutException e){
            log.warn("Element: \"" + element + "\" is not presents.");
        }
        catch (StaleElementReferenceException e){
            log.warn("Element: \"" + element + "\" is not found in the cache.");
        }

    }

    /**
     * @param theme part of subjects
     * @param content into letter
     * @param driver
     */
    public static void waitForLetterContent(String theme, String content, WebDriver driver) {
        log.info("Waiting for letter " + theme + " with " + content + "...");
        (new WebDriverWait(driver, PAGE_TIMEOUT))
                .withMessage("Letter is not found after timeout.")
                .until(((ExpectedCondition<Boolean>) d -> verifyLetterContent(theme, content)));
    }

}
