package services;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;

import static services.UrlService.refreshPage;

/**
 * Created by user
 */
@Log4j
public class CookieService {
	/**
	 * Delete all cookies.
	 * @param driver
	 */
	public static void clearCookies(WebDriver driver){
		driver.manage().deleteAllCookies();
		log.info("Delete all cookies.");
		refreshPage(driver);
	}

	/**
	 * Delete cookie
	 * @param cookieName
	 * @param driver
	 */
	public static void deleteCookie(String cookieName, WebDriver driver){
		if (driver.manage().getCookieNamed(cookieName)!=null){
			driver.manage().deleteCookieNamed(cookieName);
			log.info("Delete \""+cookieName+"\" cookie.");
		}
		else {
			log.warn("Couldn't find \""+cookieName+"\" cookie.");
		}

	}
}
