## Инструкция для проекта автоматизации тестирования Loyalty Program

#### Отчет о тестировании - https://drive.google.com/file/d/1Vr_1h_8dzwFBE_PqdJSquwULaeEtJtpv/view?usp=sharing (для пользователей templatemonster.me, если нет доступа, отчтет можно найти в корне проекта в формате pdf)
#### Документация (чек-листы, концепт автоматизации) - https://gitlab.com/tmqa5/autoMaven/wikis/home
#### Данный проект является maven-проектом на java

###### Используемые технологии:
* Java 1.8
* Maven
* Test NG
* WebDriver
* Selenoid (nodes and grid)
* Docker
* PageObject
* javax.mail
* Lombok (for using in IDEA need plugin lombok)

Для запуска тестов на компьютере понадобятся установленные:
* java 1.8
* maven
* ide (например IDEA с установленным плагином lombok)

Чтобы запустить сьюты тестов в ide:
* клонировать текущий проект
* открыть его в ide 
* перейти в дирректории autoMaven/suites
* выбрать необходимую, например loyalty-functional-suite.xml
* нажать правой клавишей и нажать run (testNG)

Чтобы запустить приемочные тесты на локальной машине:
* запустить у себя на машине webdriver с актуальным chromedriver
* в файле autoMaven/src/main/resources/properties/app.properties изменить server=localhost, port=4444
* запустить тесты

Для запуска в удаленных докер контейнерах ничего дополнительного делать не надо (должен быть досту во внутреннюю сеть), просто запустить тест и в логе будет показана 
ссылка формата - Test started on node - http://192.168.2.38:8080 перейдя по которой, через UI можно увидеть как проходят тесты через VNC

Так же тесты можно запустить в CI системе TeamCity (там все настроено и работает) - 

http://tc.templatemonsterdev.com/project.html?projectId=Monster_Tmqa5

Доступ - admin | 123123


